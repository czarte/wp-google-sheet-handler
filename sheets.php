<?php

require __DIR__ . '/vendor/autoload.php';

use Google\Spreadsheet\DefaultServiceRequest;
use Google\Spreadsheet\ServiceRequestFactory;
/**
 * google sheet handler class
 */
class GoogleHandler
{

	const APP_NAME = 'vozimeobedy sheets handler';
	const CREDENTIALS = '/credentials.json';

	/** @var Google_Service_Sheets */
	protected $client;

	/** @var custommer Lines */
	protected $custommersLines;

	/** @var custommerDrivers Lines */
	protected $custommersLinesDrivers;

	/** @var DriverDays Lines */
	protected $driverDays;


	function __construct($sheet)
	{
		/*
		 * We need to get a Google_Client object first to handle auth and api calls, etc.
		 */
		$this->client = new \Google_Client();
		$this->client->setAuthConfig(__DIR__ . self::CREDENTIALS);
		$this->client->setScopes(['https://www.googleapis.com/auth/drive','https://spreadsheets.google.com/feeds']);
		$this->custommersLinesDrivers = [];
		$this->custommersLines = [];
		$this->driverDays = [];
		$this->init($sheet);
	}

	public function init($sheet) {
		if ($this->client->isAccessTokenExpired()) {
		    $this->client->refreshTokenWithAssertion();
		}
		$accessToken = $this->client->fetchAccessTokenWithAssertion()["access_token"];
		ServiceRequestFactory::setInstance(
		    new DefaultServiceRequest($accessToken)
		);

		$this->sheets = new \Google_Service_Sheets($this->client);
  	$this->spreadsheet = (new Google\Spreadsheet\SpreadsheetService)
		   ->getSpreadsheetFeed()
		   ->getByTitle($sheet);

	}

	public function getUserData($user, $services) {
		$insert = true;
		$rows = $this->spreadsheet->getWorksheetFeed()->getByTitle('Objednávky')->getCellFeed()->toArray();

		foreach ($rows as $line => $rowdata) {
			if ($rowdata[1] == $user) {
					$insert = false;
			 		return array( 'line' => $line, 'data' => $value );
			 		break;
			}
		}
		if ($insert) {
			$userrow = $this->spreadsheet->getWorksheetFeed()->getByTitle('Objednávky')->getListFeed();

			$userrow->insert(["id" => $user, "jmeno" => $services->name, "ridic" => $services->driver]);
			$insertedRows = $this->spreadsheet->getWorksheetFeed()->getByTitle('Objednávky')->getCellFeed()->toArray();
			foreach ($insertedRows as $ikey => $ivalue) {
				if (array_search(strval($user), $ivalue, true)!== FALSE) {
					$index=$ikey;
					return array( 'line' => $index, 'data' => $value );
					break;
				}
			}
		}
  }

	public function getDriverLines($user) {
		if (!empty($this->custommersLinesDrivers[$user])) return;
		$cellFeed = $this->spreadsheet->getWorksheetFeed()->getByTitle('Řidiči')->getCellFeed()->toArray();
		foreach ($cellFeed as $line => $rowdata) {
			if ($rowdata[1] == $user) {
					$this->custommersLinesDrivers[$rowdata[1]] = $line;
			} elseif ($rowdata[1] !== 'ID') {
					$this->custommersLinesDrivers[$rowdata[1]] = $line;
			}
		}
		if (empty($this->custommersLinesDrivers[$user])) {
			$userrow = $this->spreadsheet->getWorksheetFeed()->getByTitle('Řidiči')->getListFeed();
			$userrow->insert(["id" => $user]);
			$insertedRows = $this->spreadsheet->getWorksheetFeed()->getByTitle('Řidiči')->getCellFeed()->toArray();
			foreach ($insertedRows as $ikey => $ivalue) {
				if (array_search(strval($user), $ivalue, true)!== FALSE) {
					$index=$ikey;
					$this->custommersLinesDrivers[$user] = $index;
					break;
				}
			}
		}

	}

	public function getDriverDays() {
		$cellFeed = $this->spreadsheet->getWorksheetFeed()->getByTitle('Řidiči')->getCellFeed()->toArray();
		//var_dump($cellFeed[1]); die();
		foreach ($cellFeed as $line => $rowdata) {
			if ($rowdata[1] == 'ID') {
				foreach ($rowdata as $key => $value) {
					$this->driverDays[$value] = $key;
				}
			}
		}
	}

	public function getMealKeys($dataMeals) {
		$valuesDaysMeals[0] = $this->spreadsheet->getWorksheetFeed()->getEntries()[0]->getCellFeed()->toArray()[1];
		$valuesDaysMeals[1] = $this->spreadsheet->getWorksheetFeed()->getEntries()[0]->getCellFeed()->toArray()[2];
		//var_dump($valuesDaysMeals); die();
		$mealKeys = []; //array: (key:date value: array: (key: sting[meal] value: int[cell] ) ) macht row cell number by date and meal stamp
		$iterationKey = false;
		foreach ($valuesDaysMeals[1] as $columnKey => $columnValue) {
				if (isset($valuesDaysMeals[0][$columnKey]) ) { //&& key_exists($valuesDaysMeals[0][$columnKey], array_values($dataMeals)[0])
					$iterationKey = $valuesDaysMeals[0][$columnKey];
					$mealKeys[$iterationKey][$valuesDaysMeals[1][$columnKey]] = $columnKey;
				}
				else {
					if ($valuesDaysMeals[1][$columnKey] !== "" && $valuesDaysMeals[1][$columnKey] !== "Poz." && $valuesDaysMeals[1][$columnKey] !== "driver") {
						$mealKeys[$iterationKey][$valuesDaysMeals[1][$columnKey]] = $columnKey;
					}
				}
		}
		return $mealKeys;
	}

	public function writeData($userWeek) {

			$cellFeed = $this->spreadsheet->getWorksheetFeed()->getByTitle('Objednávky')->getCellFeed();
			$batchRequest = new Google\Spreadsheet\Batch\BatchRequest();

			foreach ($userWeek as $keyUser => $dataMeals) {
				foreach ($dataMeals["meals"] as $key => $value) {

				 $cell = $cellFeed->getCell($dataMeals["line"],$key);
				 $value2write = false;

				 if ($cell !== NULL) {
					$value2write = $value > 0 ? $value : "";
					$cell->update($value2write);
				} else {
					if ($value > 0)  {
						$cellFeed->editCell($dataMeals["line"],$key, $value);
					}
				 }

				}
			}
			return "OK";
	}

	public function writeDriver($driverWeek) {

		foreach ($driverWeek as $keyWeek => $valueWeek) {
			foreach ($valueWeek as $user => $day) {
				self::getDriverLines($user);
			}
		}

		self::getDriverDays();

		$cellFeed = $this->spreadsheet->getWorksheetFeed()->getByTitle('Řidiči')->getCellFeed();

		foreach ($driverWeek as $customer => $driverData) {

			foreach ($driverData as $keyData => $data) {

				$cellFeed->editCell($this->custommersLinesDrivers[$keyData],2, $data->name);
				$cellFeed->editCell($this->custommersLinesDrivers[$keyData],3, $data->address);
				$cellFeed->editCell($this->custommersLinesDrivers[$keyData],4, $data->phone);
				$cellFeed->editCell($this->custommersLinesDrivers[$keyData],5, $data->driver);

				if ($data->payMethod == "v hotovosti") {
					$cellFeed->editCell($this->custommersLinesDrivers[$keyData],11, $data->price); // $batchRequestDriver->addEntry($cellFeed->editCell($this->custommersLinesDrivers[$keyData],10, $data->price));
				} else {
					$cellFeed->editCell($this->custommersLinesDrivers[$keyData],11, $data->payMethod); //$batchRequestDriver->addEntry($cellFeed->editCell($this->custommersLinesDrivers[$keyData],10, $data->payMethod));
				}


				foreach ($data->orders as $key => $value) {
					foreach ($value as $keyOrder => $valueOrder) {
						$cellFeed->editCell($this->custommersLinesDrivers[$keyData],$this->driverDays[$keyOrder], $valueOrder); //$batchRequestDriver->addEntry($cellFeed->editCell($this->custommersLinesDrivers[$keyData],$this->driverDays[$keyOrder], $valueOrder));
					}


				}
			}


		}
		return "OK";

	}
}
