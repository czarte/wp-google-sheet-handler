<?php

/*
  Plugin Name: Google Sheets handler plugin
  Plugin URI:
  Description:
  Text Domain: googlesheetshandler
  Version: 0.1
  Date: 2021-26-03
  Author: Vojtěch Parkán
  Author URI: http://spaceorange.eu
 */

  if( ! defined( 'ABSPATH' ) ) {
    return;
  }

  require_once (__DIR__.'/sheets.php'); //service

  class SheetHandlerPlugin
  {
    protected $handlerMeals;
    protected $handlerDrivers;

    function __construct() {
      $this->init();
    }

    private function init() {
      add_action( 'publish_objednavky', [$this, 'write_to_sheet'], 10, 1 );
    }

    function write_to_sheet($post) {
      $order_id = $post;

      $tyden = get_post_meta($order_id, 'tydendatum', true);
      $this->handlerMeals = new GoogleHandler($tyden);
      $this->handlerDrivers = new GoogleHandler("Ridici-".$tyden);

      $order_data = json_decode(get_post_meta($order_id, 'orderMeta', true));
      $compare_data = json_decode(get_post_meta($order_id, 'compareMeta', true));
      $myMealKeys = $this->handlerMeals->getMealKeys($order_data);

      $userWeek = [];
      $driverWeek = [];

      foreach ($order_data as $key => $value) {

        $userWeek[$key] = $this->handlerMeals->getUserData($key, $value->services);

        foreach ($value as $keyIn => $valueIn) {
          if ($keyIn == "services") {
            $driverWeek[] = array($key => $valueIn);
          } else {
            foreach ($valueIn as $keyIn2 => $valuein2) {
              foreach ($valuein2 as $keyMeal => $valueMeal) {
                foreach ($valueMeal as $keyPrice => $valueQty) {
                  $iKey = $myMealKeys[$keyIn][$keyIn2];
                  $oldContent = isset($userWeek[$key][$iKey]) ? intval($userWeek[$key][$iKey]) : 0;
                  $userWeek[$key]["meals"][$iKey] = $oldContent+intval($valueQty);
                }
              }
            }
          }
        }
      }
      $resultMeals = $this->handlerMeals->writeData($userWeek);
      $resultdrivers = $this->handlerDrivers->writeDriver($driverWeek);

      if($resultMeals == "OK" && $resultdrivers == "OK") {
        $argsOrder = array(
          'ID'          => $order_id,
          'post_status' => 'pending',
        );
        wp_update_post ($argsOrder);
      }

    }






  }
$handlerPlugin = new SheetHandlerPlugin();
